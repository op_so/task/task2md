# CLI Reference

This page provides documentation for the command line `task2md` application.

::: mkdocs-click
    :module: task2md.task2md
    :command: dir

::: mkdocs-click
    :module: task2md.task2md
    :command: file
