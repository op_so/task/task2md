# Overview

Version: 0.0.0

A CLI tool to generate from [Task](https://taskfile.dev/) files, some markdown
documentation files for [`mkdocs`](https://squidfunk.github.io/mkdocs-material/) static site.
